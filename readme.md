# Star wars API

Simple App to fetch and query data from Star Wars API

## Download the Repo Locally

Download the code from public bitbucket repo ``git clone https://Harris2004@bitbucket.org/Harris2004/star-wars-api.git``

## How to Run the App Locally

You can run the App in 2 ways:

### Run Locally Using Docker

- Make sure you have docker installed in your system
- Clone the repo from the link provided
- Build & run the docker image by running the following command: ``sudo docker-compose up --build
  ``
- Launch Browser to ``http://localhost:4200/``  

### Run Locally without Docker

- Clone the repo from the link provided
- Open new terminal window and browse to `frontend` directory ``cd frontend``
- install the required packages by running ``npm install``
- Build & run the Angular App by running ``ng serve``  
- Open new terminal window and browse to `backend` directory ``cd backend``
- install the required packages by running ``npm install``
- Run the express API by running ``node -r esm express.js``  
- Launch Browser to ``http://localhost:4200/``  