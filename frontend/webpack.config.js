import {webpack} from 'webpack';
module.exports = {
  //...
  plugins: [
    // Ignore all locale files of moment.js
    new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
  ],
  optimization: {
    concatenateModules: false,
  },
  devServer: {
    historyApiFallback: true,
    proxy: {
      '/api': {
        target: 'http://localhost:8080',
        secure: false,
      },
    }
  },
  watchOptions: {
    ignored: [path.posix.resolve(__dirname, './node_modules')],
  }
}
