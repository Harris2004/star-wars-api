import puppeteer from 'puppeteer'


/**
 * @name Decode using a Vin Number
 *
 * @desc fetches information about a vehicle given a vin number
 */
try {
    (async () => {
        const browser = await puppeteer.launch()
        const page = await browser.newPage()
        await page.setViewport({ width: 1280, height: 800 })
        await page.goto('https://www.localhost:4200')
        await delay(1000);
        await page.waitForSelector('.vindecoder');
        await page.click('.vindecoder');
        await page.type('.search_input', "example-vin");
        await page.click('.search_btn');
        await delay(1000);
        await browser.close()
    })()
} catch (err) {
    console.error(err)
}
