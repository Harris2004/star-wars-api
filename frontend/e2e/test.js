import puppeteer from 'puppeteer'


/**
 * @name Get Manufacturer Models
 *
 * @desc Looks for models of Honda manufacturer
 */
try {
    (async () => {
        const browser = await puppeteer.launch()
        const page = await browser.newPage()
        await page.setViewport({ width: 1280, height: 800 })
        await page.goto('https://www.localhost:4200')
        await delay(1000);
        await page.waitForSelector('.item_select')
        await page.select('.item_select', 'honda')
        await delay(1000);
        await page.select('.models_select', 'honda')
        await browser.close()
    })()
} catch (err) {
    console.error(err)
}
