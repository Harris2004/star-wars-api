module.exports = {
  preset: 'jest-preset-angular',
  roots: ['<rootDir>/src'],
  globals: {
    'ts-jest': {
      tsConfig: '<rootDir>/tsconfig.spec.json',
    },
  },
  transform: {
    '^.+\\.(ts|js|html)$': 'ts-jest',
  },
  transformIgnorePatterns: ['/node_modules/(?!@ionic|ngx-auto-unsubscribe|ng-gapi/).+\\.js$'],
  testMatch: ['<rootDir>/src/app/**/*.spec.ts'],
  testPathIgnorePatterns: [''],
  setupFilesAfterEnv: ['<rootDir>/setupJest.ts'],
  moduleNameMapper: {},
};
