module.exports = {
  launch: {
    headless: true,
    devTools: true,
    args: ['--window-size=1920,1080'],
  },
};
