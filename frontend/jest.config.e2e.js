module.exports = {
  globalSetup: './jest-puppeteer-setup.js',
  globalTeardown: './jest-puppeteer-teardown.js',
  testEnvironment: './puppeteer-environment.js',
  preset: 'jest-puppeteer',
  testMatch: [
      'e2e/test.js'
  ],
  verbose: true,
};
