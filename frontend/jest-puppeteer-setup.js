const puppeteer = require('puppeteer');
const mkdirp = require('mkdirp');
const path = require('path');
const fs = require('fs');
const os = require('os');

const DIR = path.join('e2e', 'src', os.tmpdir(), 'jest-puppeteer-global-setup/');

module.exports = async () => {
  const browser = await puppeteer.launch({
    headless: true,
    args: ['--start-maximized', '--disable-dev-shm-usage', '--incognito', '--no-sandbox', '--disable-setuid-sandbox'],
  });
  // This global is only available in the teardown but not in the TestEnvironments
  global.__BROWSER_GLOBAL__ = browser;

  // Use the file system to expose the wsEndpoint for TestEnvironments
  mkdirp.sync(DIR);
  fs.writeFileSync(path.join(DIR, 'wsEndpoint'), browser.wsEndpoint());
};
