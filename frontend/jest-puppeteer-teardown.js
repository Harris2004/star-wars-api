// ./jest-puppeteer-teardown.js

const os = require('os');
const rimraf = require('rimraf');
const path = require('path');

const DIR = path.join('e2e', 'src', os.tmpdir(), 'jest-puppeteer-global-setup');
module.exports = async () => {
  // close the browser instance:
  await global.__BROWSER_GLOBAL__.close();

  // clean up the wsEndpoint File:
  rimraf.sync(DIR);
};
