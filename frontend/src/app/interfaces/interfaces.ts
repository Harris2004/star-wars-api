export interface Person {
    id?: number;
    name?: string;
    height?: number;
    mass?: string;
    hair_color?: string;
    skin_color?: string;
    gender?: string;
    birth_year?: number,
    homePlanet?: Planet;
    personSpecies?: Array<Species>;
    personFilm?: Array<Film>;
}

export interface Planet {
    name?: string;
    terrain?: string;
    population?: number;
}

export interface Species {
    name?: string;
    average_lifespan?: string;
    classification?: string;
    language?: string;
}
export interface Film {
    title?: string;
    director?: string;
    producer?: string;
    release_date?: string;
}