import { Injectable } from '@angular/core';
import {Subject, Subscription} from "rxjs";
import {NzMessageService} from "ng-zorro-antd/message";

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  constructor(private message: NzMessageService) { }
  error(error: string) {
    this.message.create('error', error);
  }
  info(msg: string) { }
  warn(msg: string) { }
  loader(text: string): string {
    return this.message.loading(text + '..', { nzDuration: 0 }).messageId;
  }
  hideAlert(id: string) {
    this.message.remove(id);
  }
}
