import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable, throwError} from "rxjs";
import {AlertService} from "./alert.service";
import {Apollo, gql} from 'apollo-angular';
import {Person} from "../interfaces/interfaces";

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(
      private http: HttpClient,
      private apollo: Apollo,
      private alertService: AlertService) { }



  getServerResponse(data: string | number) {
    const GET_PERSON_DETAILS = gql`
  query GetPerson($personId: Int!) {
    getPerson(personId: $personId) {
      name
      height
      mass
      hair_color
      skin_color
      gender
      birth_year
      homePlanet {
        name
        terrain
        population
      }
      personSpecies {
        name
        average_lifespan
        classification
        language
      }
      personFilm {
        title
        director
        producer
        release_date
      }
    }
  }
`;
    return this.apollo
        .watchQuery<Person>({
          query: GET_PERSON_DETAILS,
          variables: {
            personId: data,
          },
        });
  }
  private handleError(shouldShowError = true) {
    return (error: any): Observable<any> => {
      let errorMessageKey;
      switch (error.status) {
        case 400:
          errorMessageKey = error.error.error;
          break;
        case 401:
          errorMessageKey = 'ERROR_401';
          break;
        case 406:
          errorMessageKey = 'ERROR_NO_DATA';
          break;
        case 409:
          errorMessageKey = 'ERROR_SAME_PASSWORD';
          break;
        case 0:
          errorMessageKey = 'ERROR_0';
          break;
        default:
          if (error.status > 400 && error.status < 500) {
            errorMessageKey = 'ERROR_4XX';
          }
          if (error.status >= 500) {
            errorMessageKey = 'ERROR_5XX';
          }
      }

      this.alertService.error(errorMessageKey);
      // TODO: if there is some issue with this change, write a new api service class
      return throwError(error);
    }
  }
}
