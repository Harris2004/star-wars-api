import { TestBed } from '@angular/core/testing';
import { AlertService } from './alert.service';
import { NzMessageServiceModule} from "ng-zorro-antd/message";

describe('AlertService', () => {
  let service: AlertService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [NzMessageServiceModule],
      providers: [
        { provide: NzMessageServiceModule, useValue: NzMessageServiceModule },
      ]
    });
    service = TestBed.inject(AlertService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
