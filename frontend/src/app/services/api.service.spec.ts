import { TestBed } from '@angular/core/testing';

import { ApiService } from './api.service';
import {HttpClientModule} from "@angular/common/http";
import {NzMessageServiceModule} from "ng-zorro-antd/message";
import {NzOverlayModule} from "ng-zorro-antd/core/overlay";
import {NgZorroAntdModule} from "../ng-zorro.module";

describe('ApiService', () => {
  let service: ApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule, NgZorroAntdModule]
    });
    service = TestBed.inject(ApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
  describe('getModelsByManufacturerId', function() {
    it('invalid input throws exception', function() {
      expect(service.getServerResponse('')).toThrowError('please provide a valid manufacturer Name')
    })
  })
});
