import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-option-select',
  templateUrl: './option-select.component.html',
  styleUrls: ['./option-select.component.scss']
})
export class OptionSelectComponent {
  @Input() data: Array<any> = [{ id: 0, value: 'Loading Data...'}]
  @Input() placeholder: string = 'Select an Entry';
  @Output() itemSelected: EventEmitter<string> = new EventEmitter();

  public selectModel: any;
  public isLoading = false;

  constructor() { }



  onSelect() {
    if (this.selectModel) {
      this.itemSelected.emit(this.selectModel);
    }
  }

}
