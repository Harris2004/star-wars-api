import {Component} from '@angular/core';
import {FormBuilder} from "@angular/forms";
import { Person } from "../../interfaces/interfaces";
import {ApiService} from "../../services/api.service";
import {AlertService} from "../../services/alert.service";



@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent {
  public allPersonsList = [
      { id: 1, value: 'Luke Skywalker'},
      { id: 2, value: 'C-3PO'},
      { id: 3, value: 'R2-D2'},
      { id: 4, value: 'Darth Vader'},
      { id: 20, value: 'Yoda'}
      ];
  public selectedPerson: string | number | null = null;
  public currentSearchInput: string = '';
  public ResData: any =  [];
  public loading:  boolean = false;
  public content: Person = {};

  constructor(private fb: FormBuilder,
              private apiService: ApiService,
              private alertService: AlertService,
              ) { }

  recordSelected(value: string | number) {
      this.selectedPerson = value;
      this.getDataByName(value);
  }
  transform(data: any): Person {
      const resp = data.data.getPerson;
      return resp;
  }

  getDataByName(value: string | number) {
    this.loading = true;
    this.apiService.getServerResponse(value).valueChanges.subscribe((result: any) => {
        this.loading = false;
        this.content = this.transform(result);
    });
  }
}
