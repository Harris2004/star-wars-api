import {
  GraphQLSchema,
  GraphQLInt,
  GraphQLObjectType,
  GraphQLString,
} from 'graphql';

const personType = new GraphQLObjectType({
  name: 'Person',
  getPersonData: async (personId) => {
    if (personId) {
      return 'test';
    }
  },
  fields: {
    id: { type: GraphQLInt },
    name: { type: GraphQLString },
    height: { type: GraphQLString },
    mass: { type: GraphQLString },
    hairColor: { type: GraphQLString },
    skinColor: { type: GraphQLString },
    gender: { type: GraphQLString },
    birthYear: { type: GraphQLInt },
    homePlanet: { type: GraphQLString }
  }
});

// planet Type
const planetType = new GraphQLObjectType({
  name: 'Planet',
  fields: {
    id: { type: GraphQLInt },
    name: { type: GraphQLString },
  }
});

const personSchema = new GraphQLSchema({ query: personType });
const planetSchema = new GraphQLSchema({ query: planetType });

export default {
  personType,
  personSchema,
  planetType,
  planetSchema,
};
