import { expect } from 'chai';
import { describe, it } from 'mocha';
import sinon from 'sinon';
import Maker from '../schema/maker';
import HttpRequester from '../libs/httprequester';

describe('Maker Tests', async () => {
  it('instance functions exist', async () => {
    const instance = new Maker();
    expect(instance.getModelNames).to.exist;
    expect(instance.getAll).to.exist;
    expect(instance.getAll).to.exist;
  });
  describe('getModelNames', async () => {
    let instance; let
      stub;
    beforeEach(async () => {
      instance = new Maker();
    });
    afterEach(async () => {
      if (stub) {
        stub.restore();
      }
    });
    it('throws error when missing input', async () => {
      try {
        await instance.getModelNames();
      } catch (err) {
        expect(err.message).to.equal('Please provide a valid Manufacturer Name');
      }
    });
    it('Retrieves Models given manufacturer Details', async () => {
      stub = sinon.stub(HttpRequester, 'get').returns({
        Results: [{
          manufactrerName: 'Honda',
          Make_ID: 1,
          Model_ID: 12,
          Model_Name: 'CR-V'
        }
        ]
      });
      const getRequest = await instance.getModelNames('honda');
      const getModels = getRequest;

      expect(stub.called).to.be.true;
      expect(getModels.length).to.equal(1);
      expect(getModels[0].value).to.equal('CR-V');
      expect(getModels[0].id).to.equal(1);
      expect(getModels[0].model_id).to.equal(12);
    });
    it('Blank/Undefined Response returns empty array', async () => {
      stub = sinon.stub(HttpRequester, 'get').returns(null);
      const getRequest = await instance.getModelNames('honda');
      expect(stub.called).to.be.true;
      expect(getRequest.length).to.equal(0);
    });
  });
  describe('getAll', async () => {
    let instance; let
      stub;
    beforeEach(async () => {
      instance = new Maker();
    });
    afterEach(async () => {
      if (stub) {
        stub.restore();
      }
    });
    it('Retrieves All records from API', async () => {
      stub = sinon.stub(HttpRequester, 'get').returns({
        Results: [{
          Make_ID: 1,
          Make_Name: 'Honda',
        }, {
          Make_ID: 2,
          Make_Name: 'Toyota',
        }
        ]
      });
      const getRequest = await instance.getAll();
      const getAll = getRequest;

      expect(stub.called).to.be.true;
      expect(getAll.length).to.equal(2);
      expect(getAll[0].value).to.equal('Honda');
      expect(getAll[0].id).to.equal(1);
      expect(getAll[1].value).to.equal('Toyota');
      expect(getAll[1].id).to.equal(2);
    });
    it('Blank Response returns empty array', async () => {
      const stub = sinon.stub(HttpRequester, 'get').returns(null);
      const getRequest = await instance.getAll();
      const getAll = getRequest;

      expect(stub.called).to.be.true;
      expect(getAll.length).to.equal(0);
    });
  });
});
