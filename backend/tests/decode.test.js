import { expect } from 'chai';
import { describe, it } from 'mocha';
import sinon from 'sinon';
import Decode from '../schema/decode';
import HttpRequester from '../libs/httprequester';

describe('Maker Tests', async () => {
  it('instance functions exist', async () => {
    const instance = new Decode();
    expect(instance.decode).to.exist;
  });
  describe('decode', async () => {
    let instance; let
      stub;
    beforeEach(async () => {
      instance = new Decode();
    });
    afterEach(async () => {
      if (stub) {
        stub.restore();
      }
    });
    it('throws error when missing input', async () => {
      try {
        await instance.decode();
      } catch (err) {
        expect(err.message).to.equal('Vin a required param');
      }
    });
    it('Retrieves Models given manufacturer Details', async () => {
      stub = sinon.stub(HttpRequester, 'post').returns({
        Results: [{
          Year: 2010,
          Make: 'Honda',
          Model: 'CR-V',
          Vin: 123
        }
        ]
      });
      const getRequest = await instance.decode('endpoint', {}, 'type');
      const getModels = getRequest;

      expect(stub.called).to.be.true;
      expect(getModels.length).to.equal(1);
      expect(getModels[0].value).to.equal('CR-V');
      expect(getModels[0].id).to.equal(1);
      expect(getModels[0].model_id).to.equal(12);
    });
    it('invalid/error vins returns empty array', async () => {

    });
  });
});
