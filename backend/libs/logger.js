import * as winston from 'winston';
import chalk from 'chalk';
import moment from 'moment';

const getTimeStamp = () => moment().format('L HH[:]mm[:]ss[.]SS');

const logFormatter = (options) => {
  let message;
  let level;
  let meta;

  ({ message, level, meta } = options);
  level = level.toUpperCase();
  message = message || '';

  if (meta && Object.keys(meta).length) {
    meta = `\n\t${JSON.stringify(options.meta)}`;
  } else {
    meta = '';
  }

  switch (level) {
    case 'info':
      level = chalk.cyan.bold(level);
      break;

    case 'warn':
      level = chalk.yellow.bold(level);
      break;

    case 'error':
      level = chalk.red.bold(level);
      break;

    default:
      break;
  }

  return `[${options.timestamp()}][${level}] ${message}${meta}`;
};

const logger = winston.createLogger({
  level: 'info',
  format: winston.format.json(),
  defaultMeta: { service: 'test-app' },
  transports: [
    new winston.transports.File({ filename: 'log/seed.log' }),
    new winston.transports.Console({
      format: winston.format.combine(winston.format.colorize()),
      timestamp: getTimeStamp,
      formatter: logFormatter,
    }),
  ],
});

export default logger;
