import axios from 'axios';
import logger from './logger';

const HttpRequester = (function requester() {
  const get = async function get(endpoint, type) {
    const data = {};
    logger.info(`Fetching All  ${type} from ${endpoint} API`);
    const res = await axios.get(endpoint, data, { headers: this.headers }).catch((error) => {
      throw new Error(`failed to connect to API ${error}`);
    });

    if (res.status === 200) {
      const resp = res.data;
      return resp;
    }
    throw new Error('failed to fetch Data from API');
  };
  const post = async function post(endpoint, data, type) {
    logger.info(`Fetching All  ${type} from API`);
    const res = await axios.post(endpoint, data, { headers: this.headers });

    if (res.status === 200) {
      const resp = res.data;
      logger.info(`Fetched data  ${type} for ${JSON.stringify(data)} from API`);
      return resp;
    }
    throw new Error('failed to fetch Data from API');
  };
  return {
    get,
    post
  };
}());

export default HttpRequester;
