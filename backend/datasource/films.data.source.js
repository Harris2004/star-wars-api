import { RESTDataSource } from 'apollo-datasource-rest';
import HttpRequester from '../libs/httprequester';

class FilmsAPI extends RESTDataSource {
  async getFilms({
    parent,
    args,
    context,
    info
  }) {
    const filmsEndpoints = parent.films;
    if (!filmsEndpoints) return null;

    const getSpeciesData = await Promise.all(filmsEndpoints.map((endpoint) =>
      HttpRequester.get(endpoint, 'Films Details')));
    return this.filmsReducer(getSpeciesData);
  }

  // eslint-disable-next-line class-methods-use-this
  filmsReducer(data) {
    return data;
  }
}

export default new FilmsAPI();
