import { RESTDataSource } from 'apollo-datasource-rest';
import HttpRequester from '../libs/httprequester';

class SpeciesAPI extends RESTDataSource {
  async getSpecies({
    parent,
    args,
    context,
    info
  }) {
    const speciesEndpoints = parent.species;
    if (!speciesEndpoints) return null;

    const getSpeciesData = await Promise.all(speciesEndpoints.map((endpoint) =>
      HttpRequester.get(endpoint, 'Species Details')));
    return this.speciesReducer(getSpeciesData);
  }

  // eslint-disable-next-line class-methods-use-this
  speciesReducer(data) {
    return data;
  }
}

export default new SpeciesAPI();
