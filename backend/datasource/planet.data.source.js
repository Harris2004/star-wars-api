import { RESTDataSource } from 'apollo-datasource-rest';
import HttpRequester from '../libs/httprequester';

class PlanetAPI extends RESTDataSource {
  async getPlanet({
    parent,
    args,
    context,
    info
  }) {
    const planetEndpoint = parent.homeworld;
    if (!planetEndpoint) throw new Error('No valid home endpoint found');
    const getPlanetData = await HttpRequester.get(planetEndpoint, 'Planet Details');

    return this.planetReducer(getPlanetData);
  }

  // eslint-disable-next-line class-methods-use-this
  planetReducer(data) {
    return data;
  }
}

export default new PlanetAPI();
