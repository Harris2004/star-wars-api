import { RESTDataSource } from 'apollo-datasource-rest';
import HttpRequester from '../libs/httprequester';

class PersonAPI extends RESTDataSource {
  async getPerson({
    args
  }) {
    const personIdValue = args.personId;
    const endpoint = `https://swapi.dev/api/people/${personIdValue}`;
    const getPersonData = await HttpRequester.get(endpoint, 'Person Details');

    return this.personReducer(getPersonData);
  }

  // eslint-disable-next-line class-methods-use-this
  personReducer(data) {
    return data;
  }
}

export default new PersonAPI();
