import express from 'express';
import { ApolloServer, gql } from 'apollo-server-express';
import logger from './libs/logger';
import UserApi from './datasource/person.data.source';
import PlanetApi from './datasource/planet.data.source';
import SpeciesApi from './datasource/species.data.source';
import FilmsApi from './datasource/films.data.source';

const app = express();
const port = 8080;

const server = new ApolloServer({
  typeDefs: gql`
    type Person {
      id: Int,
      name: String,
      height: Int,
      mass: String,
      hair_color: String,
      skin_color: String,
      gender: String,
      birth_year: String,
      homePlanet: Planet
      personSpecies: [Species]
      personFilm: [Film]
  }
  type Planet {
    name: String,
    terrain: String,
    population: String
  }
  type Species {
    name: String,
    average_lifespan: String
    classification: String
    language: String
  }
  type Film {
    title: String
    director: String
    producer: String
    release_date: String
  }
  type Query {
    getPerson(personId: Int): Person
  }
  `,
  resolvers: {
    Query: {
      getPerson: (parent, args, context, info) => UserApi.getPerson({ args })
    },
    Person: {
      homePlanet: (parent, args, context, info) => PlanetApi.getPlanet({
        parent, args, context, info
      }),
      personSpecies: (parent, args, context, info) => SpeciesApi.getSpecies({
        parent, args, context, info
      }),
      personFilm: (parent, args, context, info) => FilmsApi.getFilms({
        parent, args, context, info
      })
    }
  }
});

server.start().then(() => {
  server.applyMiddleware({ app });
  app.listen(port, () => {
    logger.info('Test App Running');
  });
});
